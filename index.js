// Real World Application Of Objects
/*
    - Scenario
        1. We would like to create a game that would have several pokemon interact with each other
        2. Every pokemon would have the same set of stats, properties and functions
*/


let pirate = {
    name : "Monkey D. Luffy",
    age : 19,
    commanders : ["Zoro","Sanji","Jimbe"] ,
    friends : {
        eastblue : ["Shanks","Dadan"],
        northblue : ["TrafalgarLaw", "Bellamy"]
    },
    talk: function() {
        console.log(`${this.commanders[0]}! I choose you to fight!`)
    }
}
console.log(pirate)
console.log("Result of dot notation:")
console.log(pirate.name)
console.log("Result of square bracket notation:")
console.log(pirate.commanders)
console.log("Result of talk method:")
pirate.talk()




function Pirate(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.attack = function(target) {
        console.log(this.name + ' attacked ' + target.name);
        let remainingHealth = target.health - this.health
        target.health = remainingHealth
        console.log(`${target.name}'s health is now reduced to ${remainingHealth}`);
        if(target.health <= 0) {
            console.log(`${target.name} is dead.`)
        }


        };
        
    this.faint = function(){
        console.log(this.name + 'fainted.');
    }

}



// Creates new instances of the "Pokemon" object each with their unique properties
let Zoro = new Pirate("Zoro", 16);
let King = new Pirate('King', 8);
let Pica = new Pirate('Pica', 6);

console.log(Zoro)
console.log(King)
console.log(Pica)
console.log(Pica.attack(Zoro))
console.log(Zoro)
console.log(Zoro.attack(Pica))
console.log(Pica)
console.log(King.attack(Zoro))
console.log(Zoro)
console.log("May plot armor si Zoro hindi po siya mamatay")
console.log(Zoro.attack(King))
console.log(Zoro.attack(King))
console.log(Zoro.attack(King))
console.log(Zoro.attack(King))
// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
// pikachu.tackle(rattata);